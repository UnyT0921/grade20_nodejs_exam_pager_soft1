'use strict';

//引用各种包
const Koa = require('koa');
const cors = require('koa-cors');
const bodyParser = require('koa-bodyparser');
const zjj = require('./router/index');

const {sync} = require('./model');
// sync();

//实例化koa
let app = new Koa();

app.use(cors());
app.use(bodyParser());
app.use(zjj());

//监听
let port = 3000;
app.listen(port);

console.log(`服务器运行在：http://localhost:${port}`);