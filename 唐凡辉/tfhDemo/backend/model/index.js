'use strict';

const { Sequelize, DataTypes, Op } = require('sequelize');
const fs = require('fs');

//链接数据库
const sequelize = new Sequelize('demo1', 'postgres', 'Tfh138139TFH', {
    host: 'unyt.top',
    dialect: 'postgres'
});
//路径
let files = fs.readdirSync(__dirname);
//筛选文件
let resfiles = files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js';
})

let obj = {};

// 遍历模型数据
resfiles.forEach(item => {
    let modelName = item.replace('.js', '');//Blog
    let tableName = modelName.toLowerCase();//blog
    console.log(modelName);
    let model = require(__dirname + '/' + item);
    obj[modelName] = sequelize.define(tableName, model);
})
// 实例化sequelize
obj.sequelize = sequelize;
obj.Op = Op;

obj.sync = async () => {
    await sequelize.sync({ force: true });
    obj.Blog.bulkCreate([

        {
            biaoti: '大苏打',
            zhaiyao: '大苏打',
            neirong: '大苏打',
            fenlei: '大苏打',
            zuozhe: '大苏打',
            fabiaoshijian: '大苏打',
        },
        {
            biaoti: '小苏打',
            zhaiyao: '小苏打',
            neirong: '小苏打',
            fenlei: '小苏打',
            zuozhe: '小苏打',
            fabiaoshijian: '小苏打',
        },

    ])


}

 obj.sync();



module.exports = obj

