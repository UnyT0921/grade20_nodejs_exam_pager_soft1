'use strict';
const { DataTypes } = require('sequelize');

let model = {

    biaoti: {
        type: DataTypes.STRING,
        allowNull: false
    },
    zhaiyao: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    neirong: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    fenlei: {
        type: DataTypes.STRING,
        allowNull: false
    },
    zuozhe: {
        type: DataTypes.STRING,
        allowNull: false
    },
    fabiaoshijian: {
        // type: DataTypes.DATE,
        type: DataTypes.STRING,
        allowNull: false
    }

}

module.exports = model;